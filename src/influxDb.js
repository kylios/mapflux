import React, { Component } from 'react'
import * as Influx from 'influx'

function withInfluxDb(host, database, WrappedComponent) {

  return class extends Component {
    state = {
      db: null
    }

    componentDidMount() {
      const db = new Influx.InfluxDB({
        host: host,
        database: database
      })

      this.setState({db})
    }

    render() {
      if (this.state.db === null) {
        return <div>Connecting...</div>
      }

      return <WrappedComponent db={this.state.db} {...this.props} />
    }
  }
}

export default withInfluxDb