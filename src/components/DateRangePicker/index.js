import React from 'react'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css';

function MyDatePicker({selected, onSelect}) {
  return <DatePicker
    selected={selected}
    onSelect={onSelect}
    showTimeSelect
    timeFormat="HH:mm"
    timeIntervals={15}
    dateFormat="LLL"
    timeCaption="time"
  />
}

function DateRangePicker({startTime, endTime, setStartTime, setEndTime}) {
  return <div>
    <h3>Start Time</h3>
    <MyDatePicker selected={startTime} onSelect={setStartTime} />
    <h3>End Time</h3>
    <MyDatePicker selected={endTime} onSelect={setEndTime} />
  </div>
}

export default DateRangePicker