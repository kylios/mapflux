import React from 'react'
import { PropTypes } from 'prop-types'
import { Map , TileLayer, Marker, Popup, Polyline } from 'react-leaflet'

function MarkerInfo({lat, lon, time, alt}) {
  const date = new Date(time)
  const dateOptions = {
    weekday: 'short',
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
    hour12: false
  }
  return <div>
    <h3>{date.toLocaleDateString('en-US', dateOptions)}</h3>
    <p>Latitude: {lat}</p>
    <p>Longitude: {lon}</p>
    <p>Altitude: {alt}</p>
  </div>
}

function MapVisualizer({data}) {

  const positions = data.map(row => [row.lat, row.lon])

  const markers = data.map((row, i) => <Marker
    key={i}
    name={row.time}
    position={[row.lat, row.lon]}
  >
    <Popup>
      <MarkerInfo time={row.time} lat={row.lat} lon={row.lon} alt={row.alt} />
    </Popup>
  </Marker>)

  const line = <Polyline positions={positions} />

  return <Map
    center={positions[0]}
    zoom={13}
    style={{height: '700px', width: '100%'}}
  >
    <TileLayer
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
    />
    {markers}
    {line}
  </Map>
}

MapVisualizer.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      lat: PropTypes.number.isRequired,
      lon: PropTypes.number.isRequired,
      alt: PropTypes.number.isRequired,
      time: PropTypes.string.isRequired
    })
  ).isRequired
}

export default MapVisualizer