import React from 'react'
import { PropTypes } from 'prop-types'
import ReactTable from 'react-table'
import 'react-table/react-table.css'

const COLUMNS = [
  {
    Header: 'Time',
    accessor: 'time',
  },
  {
    Header: 'Latitude',
    accessor: 'lat'
  },
  {
    Header: 'Longitude',
    accessor: 'lon'
  },
  {
    Header: 'Altitude',
    accessor: 'alt'
  }
]

function LocationTable({data}) {

  return <ReactTable data={data} columns={COLUMNS} />
}

LocationTable.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      lat: PropTypes.number.isRequired,
      lon: PropTypes.number.isRequired,
      alt: PropTypes.number.isRequired,
      time: PropTypes.string.isRequired
    })
  ).isRequired
}

export default LocationTable