import React, { Component } from 'react'
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs'
import moment from 'moment'
import withInfluxDb from './influxDb'
import MapVisualizer from './components/MapVisualizer'
import LocationTable from './components/LocationTable'
import DateRangePicker from './components/DateRangePicker'
import './App.css'
import 'react-tabs/style/react-tabs.css'

class App extends Component {
  state = {
    startTime: null,
    endTime: null
  }

  _setStartTime = (time) => this.setState({
    startTime: time
  })

  _setEndTime = (time) => this.setState({
    endTime: time
  })

  componentDidMount() {

    this.setState({
      startTime: moment().startOf('day'),
      endTime: moment()
    })
  }

  render() {

    return (
      <div className="App">
        <div className={"left-pane"}>
          <DateRangePicker
            startTime={this.state.startTime}
            endTime={this.state.endTime}
            setStartTime={this._setStartTime}
            setEndTime={this._setEndTime}
          />
        </div>
        <div className={"right-pane"}>
          <DataFetcher
            startTime={this.state.startTime}
            endTime={this.state.endTime}
            db={this.props.db}
          />
        </div>
      </div>
    )
  }
}

class DataFetcher extends Component {

  state = {
    data: null,
    loading: false
  }

  async componentDidUpdate(prevProps, prevState) {
    const { startTime, endTime, db } = this.props

    if (!startTime || !endTime) {
      return
    }

    if (prevProps.startTime === startTime && prevProps.endTime === endTime) {
      return
    }

    if (this.state.loading) {
      return
    }

    const query = `
      SELECT lat, lon, alt
      FROM telegraf.autogen.mqtt_consumer
      WHERE time > '${startTime.format()}' AND time < '${endTime.format()}'
    ` 

    this.setState({
      loading: true
    })

    console.log(query)

    const data = await db.query(query)
    console.log(data.length)

    this.setState({
      data: data.map(row => ({
        lat: row.lat,
        lon: row.lon,
        alt: row.alt,
        time: `${row.time}`
      })),
      loading: false
    })
  }

  render() {
    if (this.state.loading || this.state.data === null) {
      return <div>Loading...</div>
    }
    return <Data data={this.state.data} />
  }
}

function Data({data}) {
  return <div>
    <Tabs>
      <TabList>
        <Tab>Map</Tab>
        <Tab>Table</Tab>
      </TabList>
      <TabPanel>
        <MapVisualizer data={data} />
      </TabPanel>
      <TabPanel>
        <LocationTable data={data} />
      </TabPanel>
    </Tabs>
  </div>
}

export default withInfluxDb(
  'localhost',
  'autogen',
  App
)
