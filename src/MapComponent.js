import React, { Component } from 'react'
import { PropTypes } from 'prop-types'
import { Map , TileLayer, Marker, Popup, Polyline } from 'react-leaflet'


function MarkerInfo({lat, lon, time, alt}) {
  const date = new Date(time)
  const dateOptions = {
    weekday: 'short',
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
    hour12: false
  }
  return <div>
    <h3>{date.toLocaleDateString('en-US', dateOptions)}</h3>
    <p>Latitude: {lat}</p>
    <p>Longitude: {lon}</p>
    <p>Altitude: {alt}</p>
  </div>
}

function MapComponent({data, google}) {
  const markerStyle = {
    default: { fill: "#FF5722" },
    hover: { fill: "#FFFFFF" },
    pressed: { fill: "#FF5722" },
  }

  const positions = data.map(row => [row.lat, row.lon])

  const markers = data.map((row, i) => <Marker
    key={i}
    name={row.time}
    position={[row.lat, row.lon]}
  >
    <Popup>
      <MarkerInfo time={row.time} lat={row.lat} lon={row.lon} alt={row.alt} />
    </Popup>
  </Marker>)

  const line = <Polyline positions={positions} />

  return <Map
    center={positions[0]}
    zoom={13}
    style={{height: '700px', width: '100%'}}
  >
    <TileLayer
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
    />
    {markers}
    {line}
  </Map>
}

MapComponent.propTypes = {

}

export default MapComponent